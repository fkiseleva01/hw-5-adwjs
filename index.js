
const url = 'https://api.ipify.org/?format=json'
btn = document.querySelector('button')


btn.addEventListener('click', async function getIp() {
    const response = await fetch(url)
    const ip = await response.json()
    infoCreating(ip.ip)
    console.log(ip.ip);
})

async function infoCreating(ip) {
    const response = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`)
    const data = await response.json()
    fullInfoOut(data)
}

function fullInfoOut(fullInfo) {
    const div = document.createElement('div')
    div.innerHTML = (
        `<p>Континент: ${fullInfo.continent}</p>
        <p>Страна: ${fullInfo.country}</p>
        <p>Регион: ${fullInfo.regionName}</p>
        <p>Город: ${fullInfo.city}</p>
        <p>Район: ${fullInfo.district}</p>`
        )
    document.body.append(div)
}
